/*
 * structure.h
 *
 *  Created on: Mar 23, 2019
 *      Author: Minh Nguyen
 */

#ifndef STRUCTURE_H_
#define STRUCTURE_H_

#include <stdbool.h>
#define SIZE 100

typedef struct Vertex Vertex;
typedef struct Tour Tour;

int MAX_VERTICES ;
int MAX_LEVEL ;
Tour* MAX_TOUR ;

typedef struct Stack{
	int top ;
	int stack[SIZE];
}Stack;

typedef struct Vertex{
	int id;
	int cost;
	bool* successors; // Array of boolean indicating if a vertex has been visited
	Stack* nextSuccessors; /* Pointer to a stack of int containing  all successors */
	Stack* path;
	Vertex* previous;
}Vertex;

typedef struct Tour{
	Stack* path;
	int cost;
}Tour;

void toStringVertex (Vertex* vertex);
void push(Stack* stack, int element);
int pop(Stack* stack);
Vertex* createVertex(int* matrix[], Vertex* previousVertex, int nextVertexId);
Vertex** init(int* adjacencyM[]);
void printBestTour();
void copyTourInfo(Tour* bestTour);
Tour* createTour(Vertex* v);
void recursiveTour(Vertex* vertex, Tour* t, int* adja[], int level);

#endif /* STRUCTURE_H_ */
