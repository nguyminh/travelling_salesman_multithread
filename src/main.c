/*
 * main.c
 *
 *  Created on: Mar 23, 2019
 *      Author: Minh Nguyen
 */


#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdbool.h>
//#include <pthread.h>
#include <string.h>
#include "structure.h"
#include <time.h>


clock_t startm, stopm;

#define BEGIN if ( (startm = clock()) == -1) \
{ \
	printf("clock returned error."); exit(1); \
} \

#define CLOSE if ( (stopm = clock()) == -1) \
{printf("clock returned error.");\
exit(1); \
} \

#define SHOWTIME printf ( " %6.3f seconds elapsed.", ((double)stopm-startm)/CLOCKS_PER_SEC);


int main(int argc, char const *argv[])
{
	int i, j;
	int points = 0;

	if (argc != 3)
	{
		printf("Usage : ./appli.exe file.txt numberofVertices \n");
		exit;
	}
	/** -Global variable & Data structure initialisation - **/
	FILE *file;
	errno_t err;
	if (err = fopen_s(&file, argv[1], "r") != 0)
	{
		printf("error while opening");
		fflush(stdout);
		exit(0);
	}

	MAX_VERTICES = atoi(argv[2]);
	MAX_LEVEL = MAX_VERTICES - 1;

	int **matrix = malloc(MAX_VERTICES * sizeof(int*));

	for (i = 0; i < MAX_VERTICES ; ++i)
		matrix[i] = malloc(MAX_VERTICES * sizeof(int));

	/** - Open file and retrieve values from it - **/
	for (i = 0; i < MAX_VERTICES ; i++)
	{
		for (j = 0; j < MAX_VERTICES ; j++)
		{
			if (!fscanf_s(file, "%d", &matrix[i][j]))
				break;
			printf("Point [%d][%d]: %d \n",i,j, matrix[i][j]);
		}
	}
	fclose(file);

	Vertex** queue;
	queue = init(matrix);

	BEGIN;
	for( i = 0 ; i < MAX_VERTICES - 1 ; i++)
	{
		Tour* t = createTour(queue[i]);
		recursiveTour(queue[i], t, matrix, 1);
	}
	printBestTour();
	CLOSE;
	SHOWTIME;


	for(i = 0 ; i < points ; i++)
	{
		free(matrix[i]);
	}
	free(matrix);
	free(MAX_TOUR->path);
	free(MAX_TOUR);

}
