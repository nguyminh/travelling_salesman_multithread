/*
 * structure.c
 *
 *  Created on: Mar 23, 2019
 *      Author: Minh Nguyen
 */

#include <stdlib.h>
#include <stdio.h>
#include "structure.h"

#define num(x) (sizeof (x) / sizeof(*x))

/**
 * Stringify Vertex attributes
 */
void toStringVertex (Vertex* vertex)
{
	int i;
	printf("Vertex # %d \nVertex->cost : %d \n", vertex->id, vertex->cost);
	fflush(stdout);
	if(vertex->nextSuccessors->top == -1)
	{
		printf(" No more Successors to visit for this Vertex \n");
		fflush(stdout);
	}
	else
	{
		printf("Successors : ");
		for(i = 0 ; i < vertex->nextSuccessors->top + 1 ; i++)
			{
				printf(" [%d]", vertex->nextSuccessors->stack[i]);
				fflush(stdout);
			}
		printf(" \n");
	}
	printf("Vertex Path Stacks number of Elements : %d \n", vertex->path->top + 1);
	printf("Current Path: ");
	for (i = 0 ; i < vertex->path->top + 1 ; i++)
	{
		printf("[%d]", vertex->path->stack[i]);
		fflush(stdout);
	}
	printf("\n");
}

/**
 * Function to push a new element in stack.
 */
void push(Stack* stack, int element)
{
    // Check stack overflow
    if (stack->top >= SIZE)
    {
        printf("Stack Overflow, can't add more element element to stack.\n");
        fflush(stdout);
        return;
    }

    // Increase element count in stack
    stack->top++;

    // Push element in stack
    stack->stack[stack->top] = element;

    //printf("Element %d pushed to stack.\n", element);
    //printf("Vertex top = %d.\n", stack->top);
    //fflush(stdout);
}


/**
 * Function to pop element from top of stack.
 */
int pop(Stack* stack)
{
    // Check stack underflow
    if (stack->top < 0)
    {
        printf("Stack is empty.\n");
        return -1;
    }


    // Return stack top and decrease element count in stack
    return stack->stack[stack->top--];
}

/** - Calculate cost for the vertex
 *  - Set up successors array
 *  - Add vertex to path
 */


Vertex* createVertex(int* matrix[], Vertex* previousVertex, int nextVertexId){

	int i, maxVertex = MAX_VERTICES;

	Vertex* currentVertex;
	currentVertex = (Vertex*)malloc(sizeof(Vertex));
	if(currentVertex == NULL)
	{
		printf("Error: out of memory => v \n");
		return NULL;
	}
	currentVertex->id = nextVertexId;
	currentVertex->previous = previousVertex;
	currentVertex->cost = previousVertex->cost + matrix[previousVertex->id][currentVertex->id];


	printf(" *--------* \n \n Computing info for Vertex [%d] \n \n", currentVertex->id);
	printf(" Vertex Cost = previousvertex [%d]->cost :%d + matrix[%d][%d] = %d \n", previousVertex->id, previousVertex->cost, previousVertex->id, currentVertex->id, matrix[previousVertex->id][currentVertex->id]);
	fflush(stdout);

	/* Initialize successors boolean array and stack */
	/* Copy successors boolean array in current vertex.
	 * Set currentVertex->id to false in boolean array
	 * push all successors to the nextSuccessors stack
	 */
	currentVertex->successors = calloc(maxVertex, sizeof(bool));
	currentVertex->nextSuccessors = malloc(sizeof(Stack));
	currentVertex->nextSuccessors->top = -1;

	for(i = 0 ; i < maxVertex ; i++)
	{

		/* Current vertex is not one of its successors */
		if(i == currentVertex->id) currentVertex->successors[i] = false;
		else currentVertex->successors[i] = previousVertex->successors[i];
		if(currentVertex->successors[i] == true) push(currentVertex->nextSuccessors, i);
	}

	/* Initialize current Vertex Path with previous Vertex path and add this vertex */

	currentVertex->path = malloc(sizeof(Stack));
	currentVertex->path->top = -1;

	for(i = 0 ; i < previousVertex->path->top + 1 ; i++)
	{
		push(currentVertex->path, previousVertex->path->stack[i]);
	}
	push(currentVertex->path, currentVertex->id);

	/* - Debug - */
	toStringVertex(currentVertex);
	printf(" *--------* \n \n End of info computation for Vertex [%d] \n \n", currentVertex->id);
	fflush(stdout);
	return currentVertex;
}

/** -- Before splitting operations between multiple threads, the initial thread
		needs to operate on the 1st floor.
		Input : The adjacency matrix (2 Dimensions array)
		Output: @queue[N-1] with N the number of nodes
																-- **/
Vertex** init(int* adjacencyM[]){

	int i;
	int maxVertex = MAX_VERTICES;
	MAX_TOUR = malloc(sizeof(Tour));

	MAX_TOUR->cost = 50;
	MAX_TOUR->path = malloc(sizeof(Stack));

	Vertex** queue;
	queue = malloc((maxVertex - 1) * sizeof(Vertex*));
	if(queue == NULL)
	{
		printf("Error: out of memory => queue \n");
		return NULL;
	}

	Vertex* root;
	root = (Vertex*)malloc(sizeof(Vertex));
	if(root == NULL)
	{
		printf("Error: out of memory => root \n");
		return NULL;
	}
	/* Root attributes initialization */
	root->previous = NULL;
	root->cost = 0;
	root->id = 0;
	root->successors = calloc(maxVertex, sizeof(bool));
	for(i = 0 ; i < maxVertex; i ++)
	{
		if(i == 0) root->successors[i] = false;
		else root->successors[i] = true;
	}
	root->nextSuccessors = malloc(sizeof(Stack));
	root->nextSuccessors->top = -1;
	root->path = malloc(sizeof(Stack));
	root->path->top = -1;
	push(root->path, root->id);

	for(i = 1 ; i < maxVertex; i++)
	{
		push(root->nextSuccessors, i); // Adding all nodes as successors, except the 0 node
	}

	toStringVertex(root);
	for(i = 0; i < maxVertex - 1; i++)
	{
		/* --  We retrieve information for each node --*/
		queue[i] = createVertex(adjacencyM, root, i+1);
	}
	return queue;
}

void copyTourInfo(Tour* bestTour)
{
	int value;
	free(MAX_TOUR->path);
	free(MAX_TOUR);
	MAX_TOUR = malloc(sizeof(Tour));
	MAX_TOUR->cost = bestTour->cost;
	MAX_TOUR->path = malloc(sizeof(Stack));
	MAX_TOUR->path->top = -1;

	while(1)
	{
		value = pop(bestTour->path);
		if(value == -1) break;
		else
		{
			push(MAX_TOUR->path, value);
			printf("Tour Creation: pushing [%d] in tour from path \n", value);
		}
	}
}

Tour* createTour(Vertex* v)
{
	int i;
	Tour* t = malloc(sizeof(Tour));
	t->path = malloc(sizeof(Stack));
	t->path->top = -1;
	t->cost = v->cost;
	printf(" * --------- Tour Creation --------- *\n");
	/* Copy Path information from previous Vertex */
	for(i = 0 ; i < v->path->top + 1 ; i++)
	{
		push(t->path, v->path->stack[i]);
		//printf("Tour Creation: pushing [%d] in tour from path \n", v->path->stack[i]);
	}
	return t;
}
void printBestTour()
{
	int value;
	printf("Current best Tour: Cost = %d \n", MAX_TOUR->cost);
	printf("Path: ");
	while(1)
	{
		value = pop(MAX_TOUR->path);
		if(value == -1)
		{
			break;
		}
		else
		{
			printf("[%d]", value);
			fflush(stdout);
		}

	}
}
void recursiveTour(Vertex* vertex, Tour* t, int* adja[], int level)
{
	/** -- if the current vertex cost is bigger than the reference,
	 * 		we give up this tour and go back to the previous vertex,
	 * 		to start another tour
	 -- */

	if(MAX_TOUR->cost <= t->cost)
	{
		printf(" * --------- Tour Destruction --------- \n *");
		printf(" ******* Current Tour cost is higher than the Best one ******* \n \n");
		/* Give up the current Path, start a new one at previous vertex */
		free(t->path);
		free(t);
		Tour* tour = createTour(vertex->previous);
		recursiveTour(vertex->previous, tour, adja, level - 1);
		free(vertex->nextSuccessors);
		free(vertex->path);
		free(vertex);
	}
	else
	{
		/** -- Stop condition : If we came back to the root node
		 *
		 */
		if(vertex->previous == NULL)
		{
			return ;
		}
		/* if there is no successors (coming up from the bottom) */
		if(vertex->nextSuccessors->top == -1)
		{	
			/** - If we arrived at a leaf --*/
			if(level == MAX_LEVEL) 	copyTourInfo(t);
			printf(" * --------- Tour Destruction --------- \n *");
			printf(" ******* Arrived at a leaf || No more successors *******\n \n");
			free(t->path);
			free(t);
			/**-- Go back to previous vertex and compute from it
			 */
			Tour* newTour = createTour(vertex->previous);
			recursiveTour(vertex->previous, newTour, adja, level - 1);
			free(vertex->nextSuccessors);
			free(vertex->path);
			free(vertex);
		}
		else
		{
			Vertex* nextVertex = createVertex(adja, vertex, pop(vertex->nextSuccessors));
			t->cost = nextVertex->cost;
			push(t->path, nextVertex->id);

			/* -- Recursive call on the next vertex, increase level by one -- */
			recursiveTour(nextVertex, t, adja, level + 1);
		}
	}
}
