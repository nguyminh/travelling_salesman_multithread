# Hamiltonian smallest path cost problem:

- Complete graph
- There is one or more path linking all points
- Find the path linking all vertices with the smallest cost

## Launching

./hamiltonian.exe file.txt numberofVertices

### On Windows

On Windows, the execution is single-threaded.

### On Linux

At the moment, you don't specify the number of threads you want to create.
It automatically creates N-1 threads (plus the main thread) with N the number of vertices in the matrix.


For example, the following adjacency matrix (in mat.txt)


|  |  v0| v1| v2| v3| v4|
|--|----|---|---|---|---|     
|v0|   0|  3|  8|  1| 10|
|v1|   3|  0|  7|  13| 2|
|v2|   8|  7|  0|  3|  5|
|v3|   1|  13| 3| 0| 11|
|v4|   10| 2|  5|  11| 0|


Returns:
Best tour  =  0 3 2 4 1
Cost = 11

This program uses the Branch & Bound algorithm : https://en.wikipedia.org/wiki/Branch_and_bound

## Output

### Single threaded version on a 6x6 matrix

![SingleThreaded](output/singleT.PNG)

We can see the execution time is 0.549.

### Multi threaded version on the same matrix

![MultiThreaded](output/multiT.png)

We can see the execution is 0.002.


Execution time is highly reduced.
